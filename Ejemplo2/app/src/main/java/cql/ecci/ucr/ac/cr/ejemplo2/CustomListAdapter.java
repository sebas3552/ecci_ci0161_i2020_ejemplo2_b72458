package cql.ecci.ucr.ac.cr.ejemplo2;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] itemName;
    private final Integer[] imgId;
    private final String[] itemDescription;

    public CustomListAdapter(Activity context, String[] itemname, Integer[] imgid, String[] itemdescription){
        super(context, R.layout.custom_list, itemname);

        this.context = context;
        itemName = itemname;
        imgId = imgid;
        itemDescription = itemdescription;
    }

    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.custom_list, null, true);

        TextView nombre = rowView.findViewById(R.id.name);
        ImageView imagen = rowView.findViewById(R.id.icon);
        TextView descripcion = rowView.findViewById(R.id.description);

        nombre.setText(itemName[position]);
        imagen.setImageResource(imgId[position]);
        descripcion.setText(itemDescription[position]);

        return rowView;
    }
}
